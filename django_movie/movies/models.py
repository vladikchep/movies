from datetime import date
from ckeditor.fields import RichTextField
from django.db import models
from django.urls import reverse


class Category(models.Model):
    objects = models.Manager()
    name = models.CharField("Category", max_length=150)
    description = models.TextField("description")
    url = models.SlugField(max_length=160, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Category"
        verbose_name_plural = "Categories"


class Actor(models.Model):
    name = models.CharField("Name and Surname", max_length=100)
    age = models.PositiveIntegerField("age", default=0)
    description = models.TextField("description")
    image = models.ImageField("Image", upload_to="actors/")

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        return reverse('actor_detail', kwargs={"slug": self.name})

    class Meta:
        verbose_name = "Actors and directors"
        verbose_name_plural = "Actors and directors"


class Genre(models.Model):
    objects = models.Manager()
    name = models.CharField("name", max_length=100)
    description = models.TextField("description")
    url = models.SlugField(max_length=160, unique=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = "Genre"
        verbose_name_plural = "Genres"


class Movie(models.Model):
    title = models.CharField("title", max_length=100)
    tagline = models.CharField("tagline", max_length=100)
    description = models.TextField(blank=True)
    poster = models.ImageField("poster", upload_to="movies/")
    year = models.PositiveIntegerField("release date", default=2019)
    country = models.CharField("country", max_length=30)
    directors = models.ManyToManyField(Actor, verbose_name="directors", related_name="film_director")
    actors = models.ManyToManyField(Actor, verbose_name="actors", related_name="film_actor")
    genres = models.ManyToManyField(Genre, verbose_name="genres")
    world_premiere = models.DateField("Release in the world", default=date.today)
    budget = models.PositiveIntegerField("budget", default=0, help_text="sum in dollars")
    fees_in_usa = models.PositiveIntegerField("Fees in the usa", default=0, help_text="sum in dollars")
    fees_in_world = models.PositiveIntegerField("Fees in the world", default=0, help_text="sum in dollars")
    category = models.ForeignKey(Category, verbose_name="Category", on_delete=models.SET_NULL, null=True)
    url = models.SlugField(max_length=160, unique=True)
    draft = models.BooleanField("draft", default=False)
    objects = models.Manager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("movie_detail", kwargs={"slug": self.url})

    class Meta:
        verbose_name = "Film"
        verbose_name_plural = "Films"


class MovieShots(models.Model):
    title = models.CharField("title", max_length=100)
    description = models.TextField("description")
    image = models.ImageField("image", upload_to="movie_shots/")
    movie = models.ForeignKey(Movie, verbose_name="Films", on_delete=models.CASCADE)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name = "movie shot"
        verbose_name_plural = "movie shots"


class RatingStar(models.Model):
    value = models.SmallIntegerField("value", default=0)
    objects = models.Manager()

    def __str__(self):
        return f'{self.value}'

    class Meta:
        verbose_name = "rating star"
        verbose_name_plural = "rating stars"
        ordering = ["-value"]


class Rating(models.Model):
    ip = models.CharField("IP address", max_length=15)
    star = models.ForeignKey(RatingStar, on_delete=models.CASCADE, verbose_name="star")
    movie = models.ForeignKey(Movie, on_delete=models.CASCADE, verbose_name="film", related_name="ratings")
    objects = models.Manager()

    def __str__(self):
        return f"{self.star} - {self.movie}"

    class Meta:
        verbose_name = "rating"
        verbose_name_plural = "ratings"


class Reviews(models.Model):
    email = models.EmailField()
    name = models.CharField("name", max_length=100)
    text = models.TextField("massage", max_length=5000)
    parent = models.ForeignKey(
        'self', verbose_name="parent", on_delete=models.SET_NULL, blank=True, null=True
    )
    movie = models.ForeignKey(Movie, verbose_name="film", on_delete=models.CASCADE)

    def __str__(self):
        return f"{self.name} - {self.movie}"

    class Meta:
        verbose_name = "Review"
        verbose_name_plural = "Reviews"
